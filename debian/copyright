Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/systemd-cron/systemd-cron

Files: *
Copyright: © 2013 Dwayne Bent
           © 2013 Dominik Peteler
           © 2014 Konstantin Stepanov <me@kstep.me>
           © 2014 Daniel Schaal <farbing@web.de>
           © 2014-2023 Alexandre Detiste <alexandre@detiste.be>
           © 2023 наб <nabijaczleweli@nabijaczleweli.xyz>
License: MIT

Files: src/man/crontab.5.in
Copyright: 1988, 1990, 1993, 1994, Paul Vixie <paul@vix.com>
           1994, Ian Jackson <ian@davenant.greenend.org.uk>
           1996-2005, Steve Greenland <stevegr@debian.org>
           2005-2006, 2008-2012, Javier Fernández-Sanguino Peña <jfs@debian.org>
           2010-2011, 2014 Christian Kastner <debian@kvr.at>
           Numerous contributions via the Debian BTS copyright their respective authors
License: Paul-Vixie's-license

Files: src/include/libvoreutils.hpp
Copyright: наб <nabijaczleweli@nabijaczleweli.xyz>
License: 0BSD

Files: debian/*
Copyright: © 2013 Shawn Landden <shawn@churchofgit.com>
           © 2014-2023 Alexandre Detiste <alexandre@detiste.be>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 of the Software, and to permit persons to whom the Software is furnished to do
 so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: Paul-Vixie's-license
 Distribute freely, except: don't remove my name from the source or
 documentation (don't take credit for my work), mark your changes (don't
 get me blamed for your possible bugs), don't alter or remove this
 notice.  May be sold if buildable source is provided to buyer.  No
 warranty of any kind, express or implied, is included with this
 software; use at your own risk, responsibility for damages (if any) to
 anyone resulting from the use of this software rests entirely with the
 user.

License: 0BSD
 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
